package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";

	// 商品定義ファイル名
	private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";

	// 商品別集計ファイル名
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_EXIST = "定義ファイルが存在しません";
	private static final String FILE_INVALID_FORMAT = "定義ファイルのフォーマットが不正です";
	private static final String FILE_NOT_NEIGHBOURING = "売上ファイル名が連番になっていません";
	private static final String FILE_FORMAT_ERROR = "のフォーマットが不正です";
	private static final String CODE_ERROR = "コードが不正です";
	private static final String SALE_AMOUNT_OVER = "合計金額が10桁を超えました";


	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {

		//コマンドライン引数が渡されているかチェック
		if(args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}


		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();

		// 商品コードと商品名を保持するMap
		Map<String, String> commodityNames = new HashMap<>();
		// 商品コードと売上金額を保持するMap
		Map<String, Long> commoditySales = new HashMap<>();



		// 支店定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_BRANCH_LST, "支店", branchNames, branchSales, "^[0-9]{3}$")) {
			return;
		}

		// 商品定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_COMMODITY_LST, "商品", commodityNames, commoditySales, "^[A-Za-z0-9]{8}$")) {
			return;
		}



		File[] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();
		// 売上ファイルを判定し、"8桁の数字.rcd"の形式に沿っている売上ファイルだけを抽出
		for(int i = 0; i < files.length; i++) {
			String fileName = files[i].getName();

			if(files[i].isFile() && fileName.matches("^[0-9]{8}.rcd$")) {
				rcdFiles.add(files[i]);
			}
		}

		//ファイルを昇順にソートする
		Collections.sort(rcdFiles);
		//抽出した売上ファイルが連番になっているかチェック
		for(int i = 0; i < rcdFiles.size() - 1; i++) {
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0, 8));
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0, 8));

			if((latter - former) != 1) {
				System.out.println(FILE_NOT_NEIGHBOURING);
				return;
			}
		}

		//売上ファイルの読み込み処理
        for(int i = 0; i < rcdFiles.size(); i++) {
        	BufferedReader br = null;

        	try {
        		FileReader fr = new FileReader(rcdFiles.get(i));
        		br = new BufferedReader(fr);

        		List<String> saleData = new ArrayList<>();
        		String line;
        		//※readLineメソッドは指定したテキストファイルを1行ずつ読み込み、String型の戻り値として返す。
        		//Listは入れた順番で0から番号が振られていくので、saleData[0]が支店コード、saleData[1]が商品コード、saleData[2]が売上金額になる。
        		while((line = br.readLine()) != null){
        			saleData.add(line);
        		}

        		//売上ファイルが3行になっているかチェック
        		if(saleData.size() != 3) {
        			System.out.println(rcdFiles.get(i).getName() + FILE_FORMAT_ERROR);
        			return;
        		}

        		//売上ファイル内の売上金額が数字なのかチェック
        		if(!saleData.get(2).matches("^[0-9]+$")) {
        			System.out.println(UNKNOWN_ERROR);
        			return;
        		}

        		// 支店別集計処理
        		if(!calculate(rcdFiles.get(i).getName(), "の支店", saleData.get(0), saleData.get(2), branchNames, branchSales)) {
        			return;
        		}

        		// 商品別集計処理
        		if(!calculate(rcdFiles.get(i).getName(), "の商品", saleData.get(1), saleData.get(2), commodityNames, commoditySales)) {
        			return;
        		}

        	} catch(IOException e) {
    			System.out.println(UNKNOWN_ERROR);
    			return;
    		} finally {
    			// ファイルを開いている場合
    			if(br != null) {
    				try {
    					// try文が成功しようが失敗しようがファイルは閉じる必要があるので、finallyの中で閉じる。
    					br.close();
    				} catch(IOException e) {
    					System.out.println(UNKNOWN_ERROR);
    					return;
    				}
    			}
    		}
        }



		// 支店別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}

		// 商品別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_COMMODITY_OUT, commodityNames, commoditySales)) {
			return;
		}

	}

	/**
	 * 定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param エラーメッセージ表示時に使用する、定義ファイル名
	 * @param 支店コード/商品コードと、支店名/商品名を保持するMap
	 * @param 支店コード/商品コードと、売上金額を保持するMap
	 * @param 正規表現構文
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, String fileType, Map<String, String> names, Map<String, Long> sales, String formula) {
		BufferedReader br = null;

		try {
			File file = new File(path, fileName);
			//branch.lst、commodity.lstが存在するかどうかチェック
			if(!file.exists()) {
				System.out.println(fileType + FILE_NOT_EXIST);
				return false;
			}
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			// 一行ずつ読み込む
			//文字列を分割して格納
			while((line = br.readLine()) != null) {
				String[] items = line.split(",");
				//支店定義ファイル：要素は2つか、かつ、支店コードは3桁の数字か
				//商品定義ファイル：要素は2つか、かつ、商品コードは8桁の英数字か
				//※66行目、71行目で記述しているそれぞれの正規表現をString formulaに代入している。
				if(items.length != 2 || (!items[0].matches(formula))) {
					System.out.println(fileType + FILE_INVALID_FORMAT);
					return false;
				}
				//支店定義ファイル：branchNames、branchSalesに初期値を格納
				//商品定義ファイル：commodityNames、commoditySalesに初期値を格納
				names.put(items[0], items[1]);
				sales.put(items[0], 0L);

			}

		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if(br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}


	/**
	 * 集計処理
	 *
	 *@param ファイル名
	 *@param 支店/商品コード
	 *@param 売上金額
	 *@param 支店コード/商品コードと、支店名/商品名を保持するMap
	 *@param 支店コード/商品コードと、売上金額を保持するMap
	 *@return 書き込み可否
	 */
	private static boolean calculate(String fileName, String fileType, String code, String sale, Map<String, String> names, Map<String, Long> sales) {
		//Mapに特定のKeyが存在するかチェック
		if(!names.containsKey(code)) {
			System.out.println(fileName + fileType + CODE_ERROR);
			return false;
		}

		//売上金額を型変換後、支店コード・商品コードを使ってMapに加算
		long fileSale = Long.parseLong(sale);
		Long saleAmount = sales.get(code) + fileSale;

		//売上合計が11桁以上にならないようにチェック
		if(saleAmount >= 10000000000L) {
			System.out.println(SALE_AMOUNT_OVER);
			return false;
		}

		//saleAmountをMapの合計金額に置き換える
		sales.put(code, saleAmount);

		return true;
	}

	/**
	 * 集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コード/商品コードと、支店名/商品名を保持するMap
	 * @param 支店コード/商品コードと、売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> names, Map<String, Long> sales) {
		BufferedWriter bw = null;
		try {
			File file = new File(path,fileName);
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);

			//拡張For文。
			//branch.outへの出力：branchSalesのkey(支店コード)をString keyに代入し、keyの数だけ繰り返す。
			//commodity.outへの出力：commoditySalesのkey(商品コード)をString keyに代入し、keyの数だけ繰り返す。
			for(String key : sales.keySet()) {
				//支店コード, 支店名, 合計金額 が出力される(branch.out)
				//商品コード, 商品名, 合計金額 が出力される(commodity.out)
				bw.write( key + "," + names.get(key) + "," + sales.get(key));
				bw.newLine();
			}

		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			if(bw != null) {
				try {
					bw.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}

		return true;
	}

}
